// console.log('Hello!');

//Fetch keyword
	//fetch('url', {options})

	
	//GET post data

	fetch('https://jsonplaceholder.typicode.com/posts')
	.then(result => result.json())
	.then(response => {
		//the response is an array of object
		console.log(response);
		showPosts(response);
	})

console.log(document.querySelector('#div-post-entries'));
	const showPosts = (posts) =>{
		console.log(typeof	posts);

		let entries = '';

		posts.forEach((post) => {
			entries += `
			<div id = "post-${post.id}">
				<h3 id = "post-title-${post.id}">${post.title}</h3>
				<p id = "post-body-${post.id}">${post.body}</p>
				<button onclick = "editPost(${post.id})">Edit</button>
				<button onclick= "deletePost(${post.id})">Delete</button>
			</div>
			`


		})

		// console.log(entries);

		document.querySelector('#div-post-entries').innerHTML = entries;

		console.log(document.querySelector('#div-post-entries'));
	}

	//POST data on our API

document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	//when the submit event is use, we must add a parameter event to the function to capture the properties of our event.

	//to change the autoreload of the submit method
	event.preventDefault();

	//POST method
		//if we use the post request the fetch method will return the newly created document

		fetch('https://jsonplaceholder.typicode.com/posts', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				title: document.querySelector('#txt-title').value,
				body: document.querySelector('#txt-body').value,
				userId: 1
			})
		})
		.then(response => response.json())
		.then(result=> {
			console.log(result);

			alert('Post is successfully added!');

			document.querySelector('#txt-title').value = null;
			document.querySelector('#txt-body').value = null;
		})
})

	//edit post functionality
	
	const editPost = (id) => {
		console.log(id);
		
		let title = document.querySelector(`#post-title-${id}`).innerHTML;

		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		console.log(title);
		console.log(body);

		document.querySelector('#txt-edit-title').value = title;

		document.querySelector('#txt-edit-body').value = body;

		document.querySelector('#txt-edit-id').value = id;

		// the remove attribute will remove the declared attribute from the element
		document.querySelector('#btn-submit-update').removeAttribute('disabled');

	}

// updating the post
	document.querySelector('#form-edit-post').addEventListener('submit', (event) => {

		// to prevent the auto reload
		event.preventDefault();

		let id = document.querySelector('#txt-edit-id').value;

		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				title: document.querySelector('#txt-edit-title').value,
				body: document.querySelector('#txt-edit-body').value,
				userId: 1
			})

		})
		.then( response => response.json())
		.then(result => {
			console.log(result);

			alert('The post is successfully updated!');

			document.querySelector(`#post-title-${id}`).innerHTML = document.querySelector('#txt-edit-title').value;

			document.querySelector(`#post-body-${id}`).innerHTML = document.querySelector('#txt-edit-body').value;

			document.querySelector('#txt-edit-title').value = null;

			document.querySelector('#txt-edit-body').value = null;

			// to disable buttont
			// setAttribute method adds attribute to the selected element.
			document.querySelector('#btn-submit-update').setAttribute('disabled', true);

		})
	})

	// Activity
	const deletePost = (id) => {

		let post = document.querySelector(`#post-${id}`);
		post.remove();
		alert('Post has been deleted');

	}

	
