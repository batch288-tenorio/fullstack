// console.log('hello');

// Section: Document Object Model (DOM)
	// allows us to access or modify properties of an HTML element in a web page
	// it is standard on how to get, change add or delete HTML elements
	// we will be focusing only with DOM in terms of managing forms

	// using the query selector it can access the HTML elements 
		// CSS selectors to target specific element
			// ID selector(#);
			// class selector(.);
			// tag/type selector(html tags);
			// universal selector(*);
			// attribute selector([attribute]);

		// query selectors has two types: querySelector and querySelectorAll
// let firstElement = document.querySelector('#txt-first-name');
// console.log(firstElement);

	// querySelector
	let secondElement = document.querySelector('.full-name');
	console.log(secondElement);

	// querySelectorAll
	let thirdElement = document.querySelectorAll('.full-name');
	console.log(thirdElement);

	// getElements
	let element = document.getElementById('fullName');
	console.log(element);

	element = document.getElementsByClassName('full-name');
	console.log(element);

	// SECTION - Event listeners
		// whenver a user interact with a webpage, this action is considered as event
		// working with events is large part of creating interactivity in a web page
		// specific function will be invoked if the event happen

		// the function 'addEventListerner', it takes two arguments 
			// first argument a string identifying the event 
			// second argument function that the listener will invoke once the specified event occur.

		let fullName = document.querySelector('#fullName');


		let firstElement = document.querySelector('#txt-first-name');

		let txtLastName = document.querySelector('#txt-last-name');

		firstElement.addEventListener('keyup', ()=>{
			console.log(firstElement);
		
			fullName.innerHTML = `${firstElement.value} ${txtLastName.value} `
		})

		txtLastName.addEventListener('keyup', ()=> {
			console.log(txtLastName.value);

			fullName.innerHTML = `${firstElement.value} ${txtLastName.value} `
		})

		// Activity

		document.getElementById('text-color').addEventListener('change', changeColor);

		function changeColor() {
			let dropdown = document.getElementById('text-color');
			let selectedColor = dropdown.value;

			let textElement = document.getElementById('fullName');
			textElement.style.color = selectedColor;
		}

	