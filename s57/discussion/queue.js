let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection;
}

function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method
    collection[collection.length] = element;
    return collection;

}

function dequeue() {
    // In here you are going to remove the first element in the array
    
    let firstIndex = 0;

    let newArray = [];
    for(let index = firstIndex + 1; index < collection.length; index++){
        newArray[index - 1] = collection[index];
    }
    collection = newArray;
    return collection;
}

function front() {
    // you will get the first element
    return collection[0];
}

// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements   
    return collection.length;
}

function isEmpty() {
    //it will check whether the function is empty or not
    return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};