import {Container, Row, Col, Button, Form} from 'react-bootstrap';	
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

import {Link, useNavigate, Navigate} from 'react-router-dom';

import Swal2 from 'sweetalert2';

export default function Register(){
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// const [password2, setPassword2] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');

	// we consume the setUser function from the UserContext
	const {user, setUser} = useContext(UserContext);
	// we contain the useNavigate to navigate variable
	const navigate = useNavigate();

	const [isDisabled, setIsDisabled] = useState(true);

	// we have to use useEffect in enabling submit button

	useEffect(() => {
		if(email !== '' && password !== ''  && password.length >6 && mobileNumber.length >10 ){
			setIsDisabled(false);
		}
		else{
			setIsDisabled(true);
		}

	}, [email, password, mobileNumber])

	
	// function that will be triggered once we submit the form
	function register(event){
			// it prevents our pages to reload when submitting the forms
			event.preventDefault()

			fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				method : 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password,
					mobileNumber : mobileNumber
				})
			})
			.then(result=> result.json())
			.then(data => {
				if(data){
					Swal2.fire({
						title: 'Successfully Registered',
						icon: 'success',
						text: 'You have Successfully Registered '
					})

					navigate('/login')
				}
				else{
					Swal2.fire({
						title: 'Error',
						icon: 'error',
						text: 'Please check your details'
					})

				}
				
			})
			
	}


	return(
		(user.id === null)
		?
		<Container className = 'mt-5'>
			<Row>
				<Col className = 'col-6 mx-auto'>
					<h1 className = 'text-center'>Register</h1>
					<Form onSubmit = {event => register(event)}> 

						<Form.Group className="mb-3" controlId="formBasicFirstname">
						  <Form.Label>First Name</Form.Label>
						  <Form.Control 
						  	type="text" 
						  	value = {firstName}
						  	onChange = {event => setFirstName(event.target.value)}
						  	placeholder="Enter your First Name" />
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicLastname">
						  <Form.Label>Last Name</Form.Label>
						  <Form.Control 
						  	type="text" 
						  	value = {lastName}
						  	onChange = {event => setLastName(event.target.value)}
						  	placeholder="Enter your Last Name" />
						</Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control 
					        	type="email" 
					        	value = {email}
					        	onChange = {event => setEmail(event.target.value)}
					        	placeholder="Enter email" />
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicPassword">
					        <Form.Label>Password</Form.Label>
					        <Form.Control 
					        	type="password" 
					        	value = {password}
					        	onChange = {event => setPassword(event.target.value)}
					        	placeholder="Password" />
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicMobileNumber">
					        <Form.Label>Mobile Number</Form.Label>
					        <Form.Control 
					        	type="number" 
					        	value = {mobileNumber}
					        	onChange = {event => setMobileNumber(event.target.value)}
					        	placeholder="Retype your nominated password" />
					      </Form.Group>

					      <p>Have an account already? <Link to = '/login'>Log in here</Link></p>

					      <Button 
					     	 variant="primary" 
					     	 type="submit"
						     disabled = {isDisabled}>
					        Submit
					      </Button>
					    </Form>

				</Col>
			</Row>
		</Container>

		:
		<Navigate to = '/notAccessible' />

	)
}
