import coursesData from '../data/courses.js';
import CourseCard from '../components/CourseCard.js';
import {useEffect, useState} from 'react';

export default function Courses(){
	// we console the courseData to check whether we imported the mock database properly
	// console.log(coursesData);

	// the prop that will be passed will be in object data and the name of the prop will be the name of the attribute when passed.
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/activeCourses`)
		.then(result => result.json())
		.then(data => {
			// console.log(data)
			setCourses(data.map(course => {
				return(
					<CourseCard key = {course._id} courseProp = {course} />
				)
			}))
		})
	}, [])

	/*const courses = coursesData.map(course => {
			return(
				<CourseCard key = {course.id} courseProp = {course}/>
			)

	})
*/
	return(
		<>
		<h1 className = 'text-center mt-3'>Courses</h1>
		{courses}
		</>
	)
}