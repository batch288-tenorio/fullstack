import {Container, Row, Col, Card, Button} from 'react-bootstrap';

import UserContext from '../UserContext';

// import the useState hook from react
import {useState, useEffect, useContext} from 'react';

import {Link} from 'react-router-dom';

export default function CourseCard(props){
	// consume the content of the UserContext
	const {user} = useContext(UserContext);

	// console.log(props.courseProp);

	// object destructuring
	const {_id, name, description, price} = props.courseProp;

	// Use the state hook for this component to be able to store its state
	// States are use to keep track of information related to individual components

		// Syntax: const [getter, setter ] = useState(initialGetterValue);

	const [count, setCount] = useState(0);

	const [seats, setSeats] = useState(30);

	const [isDisabled, setIsDisabled] = useState(false);

	// setCount(2);
	// console.log(count);

	// this function will be invoke when the button enroll is clicked
	function enroll(){
		
		if (seats === 0){
			alert('no more seats');
		}
		else{
			setCount(count +1);
			setSeats(seats -1);
		
		}	
	}

	// The function or the side effect in our useEffect hook will invoke or run on the initial loading of our application and when there is/are change/changes on our dependencies

	// syntax:
		// useEffect(side effect, [dependencies]);
	useEffect(()=>{

		if(seats === 0){
			setIsDisabled(true);
		}

	}, [seats]);

	return(
		<Container>
			<Row>
				<Col className = 'mt-3'>
					<Card>
					     
					     <Card.Body>
					       <Card.Title>{name}</Card.Title>
					      
					         <Card.Subtitle>Description:</Card.Subtitle>
					         <Card.Text>{description} </Card.Text>

					         <Card.Subtitle>Price:</Card.Subtitle>
					         <Card.Text>Php {price}</Card.Text>

					          <Card.Subtitle>Enrollees:</Card.Subtitle>
					         <Card.Text>{count}</Card.Text>
					         {/*<p>Available seats: {seats}</p>*/}

					       
					      {

					      	user !== null
					      	?
					      	 <Button as = {Link} to = {`/courses/${_id}`} >Details</Button>
					      	 :
					      	  <Button  as = {Link} to = '/login' >Login to Enroll</Button>
					      }


					     </Card.Body>
				   </Card>


				</Col>
			</Row>
		</Container>
	)

}