
// manula import of module from package
// import Button from 'react-bootstrap/Button';
// import Container from 'react-bootstrap/Container';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

// object destructuring 
import{Button, Container, Row, Col} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Banner() {

	return(
		<Container>
			<Row>
				<Col className = 'mt-3 text-center'>
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone, everywhere!</p>
					<Button as = {Link} to = '/courses'>Enroll Now!</Button>
				</Col>
			</Row>
		</Container>
	)

}
