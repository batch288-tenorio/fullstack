import React from 'react';

// create a context object
// a context object as the name states is a data that can be used to store inforamtion that can be shared to other components within the app.

// the context object is a different approach to passing information between components and allows easier access by avoiding the use of prop passing.

// With the help of createContext() method we were able to create a context stored in variable UserContext.
const UserContext = React.createContext();

// The 'Provider' component allows other components to consume/use the context object and supply necessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;