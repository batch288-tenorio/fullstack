import './App.css';

import {useState, useEffect} from 'react';

import Home from './pages/Home.js';
import AppNavBar from './components/AppNavBar.js';
import Courses from './pages/Courses.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/Pagenotfound.js';
import CourseView from './pages/CourseView.js';

// Import UserProvider
import {UserProvider} from './UserContext.js';
/* The following line can be included in your src/index.js or App.js file */
import 'bootstrap/dist/css/bootstrap.min.css';

// The BrowserRouter component will enable us to simulate page navigation by synchronizing the show content and the shown URL in the web browser
// The routes component holds all our route components. It selects which 'route' component to show based on the url endpoint
import {BrowserRouter, Route, Routes} from 'react-router-dom';

function App() {

  let userDetails = {};
  useEffect(()=>{
    if(localStorage.getItem('token') === null){
      userDetails = {
        id: null,
        isAdmin: null
      }
    }else{
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
          method: 'GET',
          headers: {
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(result => result.json())
      .then(data => {
          userDetails = {
              id : data._id,
              isAdmin: data.isAdmin
          };

      })
    }
  }, [])
  
  
  const [user, setUser] = useState(userDetails);

  const unsetUser = () => {
    localStorage.clear();
  }
  useEffect(()=> {
    console.log(user);
    console.log(localStorage.getItem('token'));
  }, [user])


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>

        <BrowserRouter>

          <AppNavBar />

          <Routes>
            <Route path = '/' element = {<Home/>} />
            <Route path = '/courses' element = {<Courses/>} />
            <Route path = '/register' element = {<Register/>} />
            <Route path = '/login' element = {<Login/>} />*
            <Route path = '/logout' element = {<Logout/>} />
            <Route path = '/courses/:courseId' element = {<CourseView />} />
            <Route path = '*' element = {<PageNotFound />} />


             {/*{
              (user.id !== null)
              ? 
                   <Route path = '/register' element = {<PageNotFound/>} />
              :
                  <Route path = '/register' element = {<Register/>} />      
            }*/}
           
          </Routes>
      
        </BrowserRouter>
    </UserProvider>

  );
}

export default App;


